1. Explain in your own words the difference between declaring variables via `var`, `let` and `const`.
Var declarations are globally scoped or function scoped.We can update and redeclare var variables in its scope.Var hoisted to the top of their scope and initialized with undefined.
Let declaration are block scoped.They can be updated but not re-declared.Let declarations dont initialize top of their scope.
Const declaration are block scoped.We can update them but not redeclare them.They are  hoisted to the top of their scope but are not initialized.Const must be initialized during declaration.
2. Why is declaration of a variable via `var` considered a bad tone?
It does not have a block scope.This can lead to some complications.�t can be redeclare so we should be careful using it.